package remotecache;

import dataholder.DataHolder;
import initializer.ErrorSettingValue;

/**
 * Created inside IntelliJ IDEA.
 * User: kushagra.s
 * Date: 7/24/2015
 * Time: 1:30 PM
 */
public class Cache<T> implements DataHolder<T> {

    public String CACHE_ADDRESS = "<Remote_Cache_Address>";

    @Override
    public void setValue(T item) throws ErrorSettingValue {
        //set Value to remote Cache
    }

    @Override
    public int getSize() {
        //get cache size
        return 0;
    }

    public boolean isInCache( T item ) {
        //check from remote Cache
        return false;
    }
}
