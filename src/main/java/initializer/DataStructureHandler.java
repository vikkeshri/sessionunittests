package initializer;

import dataholder.DataHolder;

import java.util.Collection;

/**
 * Created inside IntelliJ IDEA.
 * User: kushagra.s
 * Date: 7/24/2015
 * Time: 1:11 PM
 */
public class DataStructureHandler<T> {

    public boolean initializeWithArray( DataHolder<T> dataHolder, T[] items ) {
        for( T item: items )
            try {
                dataHolder.setValue(item);
            } catch ( ErrorSettingValue e ) {
                return false;
            }
        return true;
    }

    public boolean initializeWithCollection( DataHolder<T> dataHolder, Collection<T> items ) {
        for( T item: items )
            try {
                dataHolder.setValue(item);
            } catch ( ErrorSettingValue e ) {
                return false;
            }
        return true;
    }

    public int getSize( DataHolder<T> dataHolder ) {
        return dataHolder.getSize();
    }
}
