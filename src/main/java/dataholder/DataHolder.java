package dataholder;

import initializer.ErrorSettingValue;

/**
 * Created inside IntelliJ IDEA.
 * User: kushagra.s
 * Date: 7/24/2015
 * Time: 1:20 PM
 */
public interface DataHolder<T> {

    public void setValue( T item ) throws ErrorSettingValue;

    public int getSize();
}
